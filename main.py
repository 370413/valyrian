#!/usr/bin/python3

__author__ = 'tom'

import wiki, csv

list_of_characters = "characters_input"

w = wiki.Wikipedia()

output = []

alias = []
csvf = open('char_data.csv','a')
wr = csv.writer(csvf, delimiter='@', quotechar='|', quoting=csv.QUOTE_MINIMAL)

f = open(list_of_characters,'r')
for line in f.readlines():
    del output[:]
    for j in range(18):
        output.append('')
    del alias[:]
    print('Alias array: ', alias)
    names = line.split()
    last_name = ' '.join(names[1:])
    output[1] = names[0]
    output[2] = last_name
    article = w.unwiki(line)
    output[17] = article[0]
    for each in article[1]:
        if each[0] == 'Spouse':
            output[14] = each[1].strip('[[').strip(']]')
        elif each[0] == 'Alias':
            alias = each[1].split('<br>')
            print('Alias array in if: ', alias)
            output[3] = alias[0]
            other_titles = '; '.join(alias)
            output[16] = other_titles
        elif each[0] == 'Title':
            output[7] = each[1].strip('[[').strip(']]')
        else:
            print(u'Unknown data value in {0:s}'.format(line))
    print(output)
    wr.writerow(output)

csvf.close()