<?php
	include_once 'functions.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Valyrian Tinfoil</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="mobile.css" media="screen and (max-width: 960px)">
	
</head>
<body>
<div id="main">

	<header>
		
	<div id="header">
	<a href="<?php echo $home ?>"><h1>Valyrian tinfoil</h1></a>
	<p class="subheader" id="subheader">or a catalogue of A Song of Ice and Fire</p>
	</div>
	
	<div id="alert_container">
		<script language="JavaScript" type="text/javascript" src="<?php echo $home ?>warning.js"></script>
	</div>
	
	</header>
	
	<nav>
	<div id="nav_main" style="width: 100%">
		<form id="search_form" method="get" action="<?php echo $home; ?>search.php">
		<ul>
			<li class="nav_button"><a href="<?php echo $home ?>">Home</a><!-- featured? --></li>
			<li class="nav_button"><a href="<?php echo $home.'browse.php' ?>">Browse</a></li>
			<li class="nav_button"><a href="<?php echo $home.'random.php' ?>">Random</a></li>
			<li class="nav_button"><a href="<?php echo $home.'characters.php' ?>">Characters</a></li>
			<li class="nav_button"><a href="<?php echo $home.'contact.php' ?>">Submit/contact</a></li>
			<li class="nav_button" id="search_nav_button"><input id="search_text_form" type="search" name="q" /><!--
				--><input id="search_button" type="submit" value="Search" /><!--
			--></li>
		</ul>
		</form>
	</div>
	</nav>
	
	<div id="content">
