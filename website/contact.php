<?php

$info = '';

if (!empty($_POST)) {
	if (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
  	$info = '<br /><br /><span style="color: #aa0000;">Invalid email format</span>'; } 
  	elseif ($_POST['captcha'] != 'J') { 
  	$info = '<br /><br /><span style="color: #aa0000;">Wrong answer for anti-spam question! You know nothing.</span>'; } 
  	else {
  	mail('tom@localhost', $_POST['subject'], $_POST['name'] . "\n\nMessage:\n" . $_POST['message'], 'From: ' . $_POST['mail'] . "\r\nReply-To: " . $_POST['mail']);
  	}
}


include 'header.php'; ?>

<div id="contact">
<form id="contact_form" method="post" action="contact.php">
	<label for="name">Name</label><br />
	<input type="text" id="name" name="name" /><br />
	<label for="mail">Email address (optional)</label><br />
	<input type="email" id="mail" name="mail" /><br />
	<label for="subject">Subject</label><br />
	<input type="text" id="subject" name="subject" /><br />	
	<label for="captcha">R + L = </label><br />
	<input type="text" id="captcha" name="captcha" /><br />
	<textarea name="message" cols="60" rows="8">Write your message here. If you want to submit someone's else theory please provide a link to it or a way to contact its author. </textarea><br />
	<input type="submit" value="Send" />
	<?php echo $info; ?>
</form>
</div>

<?php	

include 'footer.php';

?>
