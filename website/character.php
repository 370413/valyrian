<?php
	include 'header.php';
	
	if (!empty($_POST)) {
		voting($_POST['submit_button'], $_POST['item_id'], $_POST['ipaddress'], $_POST['type'], $pdo);
	}

	$result = $pdo->prepare("SELECT * FROM `valyrian`.`characters` WHERE idcharaters = :id"); // !!! Beware the misspelling in idcharacters!
	$result->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
	$result->execute();
	if ($row = $result->fetch()) { 
	if ($row['sex'] == 0) { $he_or_she = 'She'; $him_or_her = 'her'; } else { $he_or_she = 'He'; $him_or_her = 'him'; } 
	?>
	<div class="character">
	<?php include "character_header.php" ?>
		<section>
		<div class="page_description main_content"><?php echo $row['description']; ?>
			<a href="http://awoiaf.westeros.org/index.php/<?php echo $row['first_name'] . '_' . $row['last_name']; ?>"><span class="meta"> Read more on the Wiki of Ice and Fire</span></a>
		</div>
		</section>
		
		<div class="page_voting" id="char_voting_death"<?php if($row['status'] == 1) { ?> style="display: none;"<?php } ?>>
			<form method="post" id="char_vote_form_death" <?php if (has_voted($row['idcharaters'], $_SERVER['REMOTE_ADDR'], 1, $pdo)) { echo "style=\"display: none;\" "; } ?>>
				<input type="hidden" name="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
				<input type="hidden" name="item_id" value="<?php echo $row['idcharaters']; ?>" />
				<input type="hidden" name="type" value=1 />
				<input type="hidden" name="current_page" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				<button type="submit" value="1" name="submit_button" action="<?php echo $home . 'character/?id=' . $row['idcharaters']; ?>" class="against_button button"><?php echo $he_or_she; ?>'ll die</button>
				<button type="submit" value="0" name="submit_button" action="<?php echo $home . 'character/?id=' . $row['idcharaters']; ?>" class="pro_button button"><?php echo $he_or_she; ?>'ll survive the 6th book</button>
			</form>
			<?php $percent = get_result_in_percent($row['death_rating'], $row['death_votes']); echo $percent; ?>% of votes predict death
			<br />
			<img class="bar pro_bar" style="width:<?php echo ($percent*0.97).'%;'; ?>" src="<?php echo $home; ?>images/death_bar.png" />
			<img class="bar against_bar" style="width:<?php echo ((100-$percent)*0.97) . '%;'; ?>" src="<?php echo $home ?>images/pro_bar.png" />
			<br />
			(<?php echo $row['death_rating'] . '/' . $row['death_votes']; ?>)
		</div>
		
		<div class="page_voting" id="char_voting_fav">
			<form method="post" id="char_vote_form_fav" <?php if (has_voted($row['idcharaters'], $_SERVER['REMOTE_ADDR'], 2, $pdo)) { echo "style=\"display: none;\" "; } ?>>
				<input type="hidden" name="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
				<input type="hidden" name="item_id" value="<?php echo $row['idcharaters']; ?>" />
				<input type="hidden" name="type" value=2 />
				<input type="hidden" name="current_page" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				<button type="submit" value="0" name="submit_button" action="<?php echo $home . 'character/?id=' . $row['idcharaters']; ?>" class="against_button button">I don't like <?php echo $him_or_her; ?></button>
				<button type="submit" value="1" name="submit_button" action="<?php echo $home . 'character/?id=' . $row['idcharaters']; ?>" class="pro_button button">I like <?php echo $him_or_her; ?></button>
			</form>
			<?php $percent = get_result_in_percent($row['favour_rating'], $row['favour_votes']); echo $percent; ?>% of votes in favour
			<br />
			<img class="bar pro_bar" style="width:<?php echo ($percent*0.97) . '%;'; ?>" src="<?php echo $home ?>images/pro_bar.png" />
			<img class="bar against_bar" style="width:<?php echo ((100-$percent)*0.97) . '%;'; ?>" src="<?php echo $home ?>images/against_bar.png" />
			<br />
			(<?php echo $row['favour_rating'] . '/' . $row['favour_votes']; ?>)
		</div>
		
		<div class="page_info ch_info">
			<div class="info_block ch_info_block ch_rel_theories">
				<div class="info_block_part">
					<h5 class="meta">Related theories</h5>
					<ul class="rel_page_list rel_list ch_rel_theories">
						<?php echo get_related_theories_character($row['idcharaters'], $pdo); // TODO?>
					</ul>
				</div>
			</div>
			<div class="info_block ch_info_block ch_meta_info">
				<div class="info_block_part">
					<?php echo get_family($row['mother'], $row['father'], $row['spouse'], $pdo); ?>
				</div>
				<div class="info_block_part">
					<h5 class="meta">Current location:</h5>
					<ul><li><?php echo get_location($row['location'], $pdo); ?></li></ul>
				</div>
			</div>
		</div>
	</section>
	</div>
<?php	} else {
	    echo "No results";
	}

	include 'footer.php';
?>
