<?php
include 'header.php';

//if ($_POST['type'] == 0) {
//	$type = 0;
//} else { $type = 1; }

if (!empty($_POST)) {
	voting($_POST['submit_button'], $_POST['item_id'], $_POST['ipaddress'], 0, $pdo);
}


$row = get_theory_by_code($_GET['code'], $pdo);

if (!empty($row)) { 
	include ('theory_header.php');
	?>
		<header><div class="page_tldr main_content"><?php echo $row["tldr"]; ?></div></header>
		<section><div class="page_description main_content"><?php echo $row["description"]; ?></div></section>
		<?php if (!empty($row["arguments_against"])) { ?>
		<div class="page_arguments main_content">
			<h4>Arguments against:</h4>
			<section>
			<?php echo $row["arguments_against"]; ?>
			</section>
		</div>
		<?php } ?>
		<div class="page_voting">
			<form method="post" id="page_vote_form" <?php if (has_voted($row['idtheories'], $_SERVER['REMOTE_ADDR'], 0, $pdo)) { echo "style=\"display: none;\" "; } ?>>
				<input type="hidden" name="type" value=0 />
				<input type="hidden" name="ipaddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
				<input type="hidden" name="item_id" value="<?php echo $row['idtheories']; ?>" />
				<input type="hidden" name="type" value=0 />
				<input type="hidden" name="current_page" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				<button type="submit" value="0" name="submit_button" action="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="against_button button">Won't happen</button>
				<button type="submit" value="1" name="submit_button" action="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; //TODO -> character.php ?>" class="pro_button button">Will happen</button>
			</form>
			<?php $percent = get_result_in_percent($row['votes_in_favour'], $row['votes_total']); echo $percent; ?>% of votes in favour
			<br />
			<img class="bar pro_bar" style="width:<?php echo ($percent*0.97) . '%;'; ?>" src="<?php echo $home ?>images/pro_bar.png" />
			<img class="bar against_bar" style="width:<?php echo ((100-$percent)*0.97) . '%;'; ?>" src="<?php echo $home ?>images/against_bar.png" />
			<br />
			(<?php echo $row['votes_in_favour'] . '/' . $row['votes_total']; ?>)
		</div>
		<div class="page_info">
			<div class="info_block th_related_ppl">
				<div class="info_block_part">
					<h5 class="meta">Related characters</h5>
					<ul class="rel_people_list rel_list">
					<?php echo get_related_characters($row['idtheories'], $pdo); // WAIT WHAT?? TODO?>
					</ul>
				</div>
			</div>
			<div class="info_block th_supports">
				<div class="info_block_part">
					<h5 class="meta">This theory supports:</h5>
					<ul class="rel_page_list rel_list">
					<?php echo get_related_theories($row['idtheories'], 2, $pdo); ?>
					</ul>
				</div>
			</div>
			<div class="info_block th_supported_by">
				<div class="info_block_part th_requirements">
					<h5 class="meta">This theory requires following to be true:</h5>
					<ul class="rel_page_list rel_list">
					<?php echo get_related_theories($row['idtheories'], 1, $pdo); ?>
					</ul>
				</div>
				<div class="info_block_part th_supported_by_not_req">
					<h5 class="meta">This theory is supported by:</h5>
					<ul class="rel_page_list rel_list">
					<?php echo get_related_theories($row['idtheories'], 0, $pdo); ?>
					</ul>
				</div>
			</div>
			<div class="info_block th_meta">
				<div class="info_block_part th_category">
					<span class="meta">Category: </span>
					<a href="<?php echo $home . 'category/' . $row['category']; ?>"><?php echo $row['category_name']; ?></a>
				</div>
				<div class="info_block_part th_date">
					<span class="meta">Added: </span><?php echo $row['date']; ?>
				</div>
				<div class="info_block_part th_original">
					<?php if (!empty($row['original'])) { ?><a href="<?php echo $row['original']; ?>"><span class="meta">Original author</span><?php if (!empty($row['author'])) { ?>: <br /><?php echo $row['author']; } ?></a><?php } else { ?><span class="meta">Author unknown</span><?php } ?>
				</div>
				<div class="info_block_part th_report">
					<a href="<?php echo $home; ?>contact.php?subject=<?php echo $row['theory_code']; ?>"><span class="meta">Report/contact</span></a>
				</div>
			</div>
		</div>		
	</div>
	</section>
<?php	}
	
 else {
    echo "No results";
}


include 'footer.php';
?>
