<?php

include_once 'functions.php';

if (empty($_GET['q'])) {
	header("Location: $home");
	die();
}

$q = $_GET['q'];

$result = $pdo->prepare("SELECT `theory_code` FROM `valyrian`.`theories` WHERE theory_code = :word or `name` = :word");
$result->bindValue(':word', $_GET['q']);
$result->execute();
if ($row = $result->fetch()) { header("Location: {$home}theory/{$row['theory_code']}"); die(); }

include 'browse.php';

?>
