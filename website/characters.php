<?php
include 'header.php';
		
if (!empty($_GET['theory'])) { $theory = $_GET['theory']; } else { $theory = 0; };
if (!empty($_GET['location'])) { $location = $_GET['location']; } else { $location = 0; };	
if (!empty($_GET['hide_bastards'])) { $hide_bastards = $_GET['hide_bastards']; } else { $hide_bastards = false; };
if (!empty($_GET['hide_minor'])) { $hide_minor = $_GET['hide_minor']; } else { $hide_minor = false; };
if (!empty($_GET['hide_dead'])) { $hide_dead = $_GET['hide_dead']; } else { $hide_dead = false; };
if (!empty($_GET['sort_by'])) { $sort_by = $_GET['sort_by']; } else { $sort_by = 1; }; // necessary?
if (!empty($_GET['sort_order'])) { $sort_order = $_GET['sort_order']; } else { $sort_order = 'DESC'; };
if (!empty($_GET['start'])) { $start = intval($_GET['start']); } else { $start = 0; }
if (!empty($_GET['show'])) { $show = intval($_GET['show']); } else { $show = 15; }
$end = $start+$show;
	
if (!empty($_GET)) {
	$query = "SELECT * FROM `valyrian`.`characters` ";
	if ($theory != '0') { 
		$query .= "INNER JOIN `valyrian`.`theories_characters` ON `valyrian`.`characters`.`idcharaters` = `valyrian`.`theories_characters`.`character` WHERE `theory` = :theory"; } 
	else { $query .= "WHERE true"; }
	if ($location != '0') { 
		$query .= " and `location` = :location"; }
	if ($hide_bastards == 'on') {
		$query .= " and `legitimate` = 1";
	}
	if ($hide_minor == 'on') {
		$query .= " and `important` = 1";
	}	
	if ($hide_dead == 'on') {
		$query .= " and not `status` = 1";
	}	
	switch ($sort_by) {
		case 7:
			$query .= " and not last_name = '' ORDER BY last_name";
			break;
		case 6:
			$query .= " and not first_name = '' ORDER BY first_name";
			break;
		case 5:
			$query .= " ORDER BY (death_rating / death_votes)";
			break;
		case 4:
			$query .= " ORDER BY death_rating";
			break;
		case 3:
			$query .= " ORDER BY (favour_rating / favour_votes)";
			break;
		case 2:
			$query .= " ORDER BY favour_rating";
			break;
		case 1:
		default:
			$query .= " ORDER BY death_votes";

	}
	if ($sort_order == 'ASC') { $sort_order = 'ASC'; } else { $sort_order = 'DESC'; }
	$query .= " $sort_order LIMIT :start, :end";
} else { $query = "SELECT * FROM `valyrian`.`characters` WHERE `important` = 1 LIMIT 15"; $hide_minor = 'on'; }

try {
	$result = $pdo->prepare($query);
	if ($theory != '0') { $result->bindValue(':theory', $theory); }
	if ($location != '0') { $result->bindValue(':location', $location); }
	$result->bindValue(':start', $start, PDO::PARAM_INT);
	$result->bindValue(':end', $end, PDO::PARAM_INT);
	$result->execute();
} catch (PDOException $e) { echo $e->getMessage(); }
// filter and sorting :)
?>
	
	<nav>
	<div class="browse_nav meta">
	<form id="browse_nav_form" method="get" action="<?php echo $home.'characters.php' ?>">
		<div class="browse_nav_part filter">
			Location: <br />
			<select name="location">
				<option value='0' <?php if ($location == 0) {?>selected<?php } ?>>All</option>
				<?php echo get_menu_of_locations($pdo, $location); ?>
			</select>
		</div>
		<div class="browse_nav_part filter">
			Related theory: <br />
			<select name="theory">
				<optgroup label="Any">
				<option value=0 <?php if ($theory == 0) {?>selected<?php } ?>>All</option>
				<?php echo get_menu_of_theories($pdo, $theory); ?>
			</select>
		</div>
		<div class="browse_nav_part sorting">
			Sort: <br />
			<select name="sort_by">
				<option value=1 <?php if ($sort_by == 1) { ?>selected<?php } ?>>Popularity (number of votes)</option>
				<option value=2 <?php if ($sort_by == 2) { ?>selected<?php } ?>>Number of votes in favour</option>
				<option value=3 <?php if ($sort_by == 3) { ?>selected<?php } ?>>Percent of votes in favour</option>
				<option value=4 <?php if ($sort_by == 4) { ?>selected<?php } ?>>Number of votes predicting death</option>
				<option value=5 <?php if ($sort_by == 5) { ?>selected<?php } ?>>Percent of votes predicting death</option>
				<option value=6 <?php if ($sort_by == 6) { ?>selected<?php } ?>>First name</option>
				<option value=7 <?php if ($sort_by == 7) { ?>selected<?php } ?>>Last name</option>
			</select>
			<select name="sort_order">
				<option value='ASC'>Ascending</option>
				<option value='DESC' <?php if ($sort_order == 'DESC') { ?>selected<?php } ?>>Descending</option>
			</select>
			<br />
			<label for="hide_bastards">Hide natural children</label>
			<input type="checkbox" id="hide_bastards" name="hide_bastards" <?php if ($hide_bastards == 'on') { ?>checked<?php } ?> />
			<br />
			<label for="hide_minor">Hide unimportant characters</label>
			<input type="checkbox" id="hide_minor" name="hide_minor" checked="<?php if ($hide_minor == 'on') { ?>true<?php } else { ?>false<?php } ?>" />
			<br />
			<label for="hide_dead">Hide dead characters</label>
			<input type="checkbox" id="hide_dead" name="hide_dead" <?php if ($hide_dead == 'on') { ?>checked<?php } ?> />
			<br />
			<input type="submit" value="OK" />
		</div>
	</form>
	</div>
	</nav>
	
	<?php
	
	$i = 0;
	if ($row = $result->fetch()) {
	do {
		?>
		<div class="character">
		<?php 
		$i++;
		include ('character_header.php'); ?>
		
		<div class="loop_bar"<?php if($row['status'] == 1) { ?> style="display: none;"<?php } ?>>
			<?php $percent = get_result_in_percent($row['death_rating'], $row['death_votes']); echo $percent; ?>% votes predict death
			(<?php echo $row['death_rating']; ?>/<?php echo $row['death_votes']; ?>) <br />
			<img class="bar pro_bar" style="width:<?php echo ($percent*0.97) . '%;'; ?>" src="<?php echo $home ?>images/death_bar.png" />
			<img class="bar against_bar" style="width:<?php echo ((100-$percent)*0.97) . '%;'; ?>" src="<?php echo $home ?>images/pro_bar.png" />
		</div>
		</div>
		</section><?php
	} while($row = $result->fetch());
	} else { echo "<div class=\"noresults\">No results</div>"; }
	
	$browse_page = $home . "characters.php?theory=" . $theory . "&location=" . $location . "&sort_by=" . $sort_by . "&sort_order=" . $sort_order . "&show=" . $show . "&hide_bastards=" . $hide_bastards . "&hide_minor=" . $hide_minor . "&hide_dead=" . $hide_dead;
	
	include 'pagenav.php';
	
	include 'footer.php'
?>
