<?php
include 'header.php';
		
if (!empty($_GET['character'])) { $character = $_GET['character']; } else { $character = 0; };
if (!empty($_GET['category'])) { $category = $_GET['category']; } else { $category = 0; };	
if (!empty($_GET['sort_by'])) { $sort_by = $_GET['sort_by']; } else { $sort_by = 1; }; // necessary?
if (!empty($_GET['sort_order'])) { $sort_order = $_GET['sort_order']; } else { $sort_order = 'DESC'; };
if (!empty($_GET['start'])) { $start = intval($_GET['start']); } else { $start = 0; }
if (!empty($_GET['show'])) { $show = intval($_GET['show']); } else { $show = 15; }
$end = $start+$show;
	
if (!empty($_GET)) {
	$query = "SELECT theory_code, `name`, `tldr`, `status`, `votes_in_favour`, `votes_total`, `category` FROM `valyrian`.`theories` ";
	if ($character != '0') { 
		$query = $query . "INNER JOIN `valyrian`.`theories_characters` ON `valyrian`.`theories`.`idtheories` = `valyrian`.`theories_characters`.`theory` WHERE `character` = :character"; } 
	else { $query = $query . "WHERE true"; }
	if ($category != '0') { 
		$query = $query . " and `category` = :category"; }
	if (isset($q)) {
		$query = $query . " and (`tldr` LIKE :q or `description` LIKE :q or `name` LIKE :q)";
	}
	switch ($sort_by) {
		case 3:
			$query = $query . " ORDER BY (votes_in_favour / votes_total)";
			break;
		case 2:
			$query = $query . " ORDER BY votes_in_favour";
			break;
		case 1:
		default:
			$query = $query . " ORDER BY votes_total";

	}
	if ($sort_order == 'ASC') { $sort_order = 'ASC'; } else { $sort_order = 'DESC'; }
	$query = $query . " $sort_order LIMIT :start, :end";
} else { $query = "SELECT theory_code, `name`, `tldr`, `status`, `votes_in_favour`, `votes_total`, `category` FROM `valyrian`.`theories` LIMIT 15"; }

try {
	$result = $pdo->prepare($query);
	if ($character != '0') { $result->bindValue(':character', $character); }
	if ($category != '0') { $result->bindValue(':category', $category); }
	if (isset($q)) { $result->bindValue(':q', "%{$q}%"); }
	$result->bindValue(':start', $start, PDO::PARAM_INT);
	$result->bindValue(':end', $end, PDO::PARAM_INT);
	$result->execute();
} catch (PDOException $e) { echo $e->getMessage(); }
// filter and sorting :)
	

?>
	
	<nav>
	<div class="browse_nav meta">
	<form id="browse_nav_form" method="get" action="<?php echo $home.'browse.php' ?>">
		<div class="browse_nav_part filter">
			Category: <br />
			<select name="category">
				<option value='0' <?php if ($category == 0) {?>selected<?php } ?>>All</option>
				<?php echo get_menu_of_categories($pdo, $category); ?>
			</select>
		</div>
		<div class="browse_nav_part filter">
			Featured character: <br />
			<select name="character">
				<optgroup label="Any">
				<option value=0 <?php if ($character == 0) {?>selected<?php } ?>>All</option>
				<?php echo get_menu_of_characters($pdo, $character); // also add by status ?>
			</select>
		</div>
		<div class="browse_nav_part sorting">
			Sort: <br />
			<select name="sort_by">
				<option value=1 <?php if ($sort_by == 1) { ?>selected<?php } ?>>Popularity (number of votes)</option>
				<option value=2 <?php if ($sort_by == 2) { ?>selected<?php } ?>>Number of votes in favour</option>
				<option value=3 <?php if ($sort_by == 3) { ?>selected<?php } ?>>Percent of votes in favour</option>
			</select>
			<select name="sort_order">
				<option value='ASC'>Ascending</option>
				<option value='DESC' <?php if ($sort_order == 'DESC') { ?>selected<?php } ?>>Descending</option>
			</select>
			<input type="submit" value="OK" />
		</div>
	</form>
	</div>
	</nav>
	
	<?php
	
	include 'loop.php'; 
	
	$browse_page = $home . "browse.php?character=" . $character . "&category=" . $category . "&sort_by=" . $sort_by . "&sort_order=" . $sort_order . "&show=" . $show;	
	
	include 'pagenav.php';
	
	include 'footer.php'
?>
