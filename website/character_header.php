<section> 
		<header>
		<div class="page_header" id="page_header_1">
			<div class="page_h_code">
				<a href="<?php echo $home . 'character/' . $row['idcharaters']; ?>">
				<?php echo $row['idcharaters']; ?>
				</a>
			</div>
			<div class="page_h_name">
				<a href="<?php echo $home . 'character/' . $row['idcharaters']; ?>">
				<h3>
				<?php echo $row["first_name"] . " " . $row["last_name"];
				if (!empty($row['common_name'])) {
					echo " (" . $row['common_name'] . ")";
				} ?>
				</h3>
				</a>
			</div>
			<div class="page_h_status"><span class="status_label meta">Status:</span> <?php echo get_char_status($row['status']); ?></div>
		</div>		
		<div class="page_tldr main_content"><?php echo $row['title']; ?></div>
		<?php if(!empty($row['other_titles'])) { ?>
		<div class="character_other_titles main_content"><span class="meta">Also known as: </span> <?php echo $row['other_titles']; ?></div>
		<?php } ?>
		</header>
