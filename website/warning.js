/////////////////////////////////////////////////////////////
//
// Author Scott Herbert (www.scott-herbert.com)
//
// Version History 
// 1 (10-Feb-2013) Inital release on to GitHub.
//
// Download from http://adf.ly/IvElY

function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
  {
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)
    {
    return unescape(y);
    }
  }
}


function displayNotification()
{

// this is the message displayed to the user.
var message = "<div id=\"spoiler_alert\">Warning! This site contains spoilers of all published books from 'A Song of Ice and Fire'! It also uses cookies, like most websites do (But I'm legally obliged to warn you so here we go).<INPUT TYPE='button' VALUE=\"I don\'t mind\" onClick='JavaScript:doAccept();' /> <INPUT TYPE='button' VALUE=\"Take me away!\" onClick='JavaScript:doNotAccept();' /></div>";
var alert = document.getElementById('alert_container');
alert.innerHTML = message;
}

function doAccept() {
    setCookie("jsCookieCheck", null, 3650);
    location.reload(true);
}

function doNotAccept() {
	window.location.replace("https://wikipedia.org/");
}

function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;

// set cookiewarning to hidden.
var cw = document.getElementById("alert_container");
cw.innerHTML = "";
}

function checkCookie() {

    var cookieName = "jsCookieCheck";
    var cookieChk = getCookie(cookieName);
    if (cookieChk != null && cookieChk != "") {
        // the jsCookieCheck cookie exists so we can assume the person has read the notification
        // within the last year and has accepted the use of cookies

        //setCookie(cookieName, cookieChk, 3650); // set the cookie to expire in a year. (10 yrs)
    }
    else {
        // No cookie exists, so display the lightbox effect notification.
        displayNotification();
    }
}

// blockOrCarryOn - 1 = Carry on, store a do not store cookies cookie and carry on
//0 = Block, redirect the user to a different website (google for example)				
checkCookie();


