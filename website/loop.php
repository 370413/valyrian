<?php
$i = 0;
if ($row = $result->fetch()) {
//if ($result->num_rows > 0) {
	// output data of each row
//try {	
	do {
		$i++;
		include ('theory_header.php');
		if ($row["tldr"] != null) { ?>
			<div class="theory_main page_main">
				<div class="theory_tldr page_tldr main_content"><?php echo $row["tldr"]; ?></div>
			</div>
	<?php	} ?>
		<div class="loop_bar">
			<?php $percent = get_result_in_percent($row['votes_in_favour'], $row['votes_total']); echo $percent; ?>% votes
			(<?php echo $row['votes_in_favour']; ?>/<?php echo $row['votes_total']; ?>) <br />
			<img class="bar pro_bar" style="width:<?php echo ($percent*0.97) . '%;'; ?>" src="<?php echo $home ?>images/pro_bar.png" />
			<img class="bar against_bar" style="width:<?php echo ((100-$percent)*0.97) . '%;'; ?>" src="<?php echo $home ?>images/against_bar.png" />
		</div>
		</div>
		</section><?php
	} while($row = $result->fetch());
//} catch(PDOException $e) { echo "No results" . $e->getMessage(); }
} else { echo "<div class=\"noresults\">No results</div>"; }

?>
