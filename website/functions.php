<?php

// establishing a connection

	$home = "/ASOIAFFanTheoriesCatalogue/";
	$username = 'tinfoil_server';
	$password = 'valyrian';
	$database = 'valyrian';
	$servername = 'localhost';

	try
	{
		$pdo = new PDO("mysql:host=" . $servername . ";dbname=" . $database, $username, $password);
		//echo 'Połączenie nawiązane!';
	}
	catch(PDOException $e)
	{
		echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
	}
	
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
// functions

function get_char_link($id, $first_name, $last_name, $common_name) {
	global $home;
	if (!empty($common_name)) { $pseudo = " ($common_name)"; }
	return "<a href=\"{$home}character/$id\">$first_name {$last_name}{$pseudo}</a>";
}

function get_all_theories($con) {
	try { return $con->query("SELECT * FROM `valyrian`.`theories`"); }
	catch(PDOException $e) { echo $e->getMessage; }
}

function get_theory_status($status) {
	switch ($status) {
		case 0:
			return "Unconfirmed";
			break;
		case 1:
			return "<span class=\"status_good\">Suggested</span>";
			break;
		case 2:
			return "<b style=\"color:#cc0000\">Proven false</b>";
			break;
		case 3:
			return "<b style=\"color:#cc6600\">Shown to be partially true</b>";
			break;
		case 4:
			return "<b class=\"status_good\">Proven true</b>"; 
			break;
	}
}

function has_voted($item_id, $ipaddress, $type, $con) {
	$query = $con->prepare("SELECT * FROM `valyrian`.`votes` WHERE IP = :ipaddress and item = :itemid and itemtype = :itemtype");
	$query->bindValue(':ipaddress', $ipaddress);
	$query->bindValue(':itemid', $item_id, PDO::PARAM_INT);
	$query->bindValue(':itemtype', $type, PDO::PARAM_INT);
	$query->execute();
	$result = $query->fetch();
	$query->closeCursor;
	if (empty($result)) {
		return FALSE;
	} else {
		return TRUE;
	}
}

function add_vote($item_id, $ipaddress, $type, $vote, $con) {
	$queries = array();
	switch ($type) {
		case 0:
			if ($vote != 0) { 
			$queries[1] = "UPDATE `valyrian`.`theories` SET votes_in_favour = votes_in_favour+1 WHERE idtheories = :itemid";
			} 
			
			$queries[2] = "UPDATE `valyrian`.`theories` SET votes_total = votes_total+1 WHERE idtheories = :itemid";
			break;
			
		case 1:
			if ($vote != 0) { 
				$queries[1] = "UPDATE `valyrian`.`characters` SET death_rating = death_rating+1 WHERE idcharaters = :itemid;";
			} 
			
			$queries[2] = "UPDATE `valyrian`.`characters` SET death_votes=death_votes+1 WHERE idcharaters = :itemid";
			break; 
			
		case 2:
			if ($vote != 0) { 
				$queries[1] = "UPDATE `valyrian`.`characters` SET favour_rating = favour_rating+1 WHERE idcharaters = :itemid;";
			} 
			
			$queries[2] = "UPDATE `valyrian`.`characters` SET favour_votes=favour_votes+1 WHERE idcharaters = :itemid";
			break; 
			
	}// CZYSZCZENIE - INFORMACJA O LICZBIE GŁOSÓW POZYTYWNYCH I CAŁKOWITYCH NA ITEM SIĘ POWTARZA!!! - DEBILNY KOD WYSZEDŁ, SZCZERZE POWIEDZIAWSZY, PRZEZ TO :(
	$queries[3] = "INSERT INTO `valyrian`.`votes` VALUES (:ipaddress, :itemid, :itemtype, :vote)";
	for ($i=1; $i<4; $i++) {
		if (!empty($queries[$i])) {
			try { $query = $con->prepare($queries[$i]);
			$query->bindValue(':itemid', $item_id, PDO::PARAM_INT);
			if ($i == 3) {
				$query->bindValue(':ipaddress', $ipaddress);
				$query->bindValue(':vote', $vote, PDO::PARAM_INT);
				$query->bindValue(':itemtype', $type, PDO::PARAM_INT);
			}
			$query->execute(); }
		catch (PDOException $e) { echo $e->getMessage(); }
		}
	}
}

function voting($button, $itemid, $ipaddress, $type, $con) {
	if ($button == 1) { $vote = 1; } elseif ($button == 0) { $vote = 0; }
	$items = array($itemid);
	$req_theories = $con->prepare("SELECT `idtheories` FROM `valyrian`.`theories` JOIN `valyrian`.`related_theories` ON theory_2 = `idtheories` WHERE theory_1 = :id and relation_type = 1;");
	$req_theories->bindValue(':id', $itemid, PDO::PARAM_INT);
	$req_theories->execute();
	while ($row = $req_theories->fetch()) {
		$items[] = $row['idtheories'];
	}
	foreach ($items as $item){
	if (!has_voted($item, $ipaddress, $type, $con)) {
		add_vote($item, $ipaddress, $type, $vote, $con);
	} else {
		echo "You cannot vote twice!";
	}
	}
	$req_theories->closeCursor();
	return TRUE;
}

function get_arguments_as_list($th, $type, $con) {
	$output = "None";
	$query = $con->prepare("SELECT * FROM `valyrian`.`arguments` WHERE theory = :th and type = :type");
	$query->bindValue(':th', $th, PDO::PARAM_INT);
	$query->bindValue(':type', $type, PDO::PARAM_INT);
	$query->execute();
	$result = $query->fetchAll();
	$query->closeCursor();
	if (!empty($result)) {
		$output = "";
	    	// output data of each row
	    	foreach($result as $row) {
	    		$output = $output . "<li>" . $row['text'] . "</li>";
	    	}
	}
	return $output;  
}

function get_theory_by_code($code, $con) {
	$query = $con->prepare("SELECT * FROM `valyrian`.`theories` INNER JOIN `valyrian`.`categories` ON category = idcategories WHERE theory_code = :code"); 
	$query->bindValue(':code', $code);
	$query->execute();
	$output = $query->fetch();
	$query->closeCursor();
	return $output;
}

function get_result_in_percent($fav, $total) {
	if ($total != 0 ) {
		return round(100*$fav/$total);
	} else {
		return '?';
	}
}

function get_related_theories($id, $rel_type, $con) {	
	global $home;
	switch ($rel_type) {
		case 0:
			$column = 1;
			$col2 = 2;
			$q_ending = " and relation_type = 0;";
			break;
		case 1:
			$column = 1;
			$col2 = 2;
			$q_ending = " and relation_type = 1;";	
			break;
		case 2:	
			$column = 2;
			$col2 = 1;
			$q_ending = ";";
			break;
	}
	
	$query = "SELECT `name`, `theory_code`, `status` FROM `valyrian`.`theories` JOIN `valyrian`.`related_theories` ON theory_" . $col2 . " = `idtheories` WHERE theory_$column = :id$q_ending";
	try {
		$result = $con->prepare($query);
		$result->bindValue(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$output = '';
		if ($row = $result->fetch()) {
			do {
				$output = $output . "<li class=\"rel_theory rel_theory_$rel_type rel_theory_$status\"><a href=\"$home"."theory/".$row['theory_code']."\">".$row['name']."</a></li>\n";
			} while($row = $result->fetch());
		} else {
			$output = '<li>None</li>';
		}
		$query->closeCursor;
	} catch (PDOException $e) { $output = $e->getMessage(); }
	return $output;
}

function get_menu_of_categories($con, $get) { 
	$query = 'SELECT `idcategories`, `category_name` FROM `valyrian`.`categories`;';
	$result = $con->query($query);
	$output = '';
	foreach ($result as $row) {

		$output = $output . "<option value=" . $row['idcategories'];
		if ($row['idcategories'] == $get) { $output = $output . " selected"; }
		$output = $output . ">" . $row['category_name'] . "</option>";
	}
	return $output;
}

function get_menu_of_characters($con, $get) {
	// no need to prepare
	$query = 'SELECT `idcharaters`, `first_name`, `last_name`, `common_name`, `location`, `location_name` FROM `valyrian`.`characters` INNER JOIN `valyrian`.`locations` ON `location` = `idlocations` WHERE `important` = 1 ORDER BY `location` ASC, `first_name` ASC;';
	$result = $con->query($query);
	$output = '';
	$loc = 0;
	foreach ($result as $row) {
		if ($row['location'] != $loc) {
			$output = $output . "</optgroup>\n<optgroup label=\"" . $row['location_name'] . "\">";
			$loc = $row['location'];
		}
		$output = $output . "<option value=" . $row['idcharaters'];
		if ($row['idcharaters'] == $get) { $output = $output . " selected"; }
		$output = $output . ">" . $row['first_name'] . " " . $row['last_name'];
		if ($common_name != '') {
			$output = $output . " (" . $row['common_name'] . ")";
		}
		$output = $output . "</option>";
	}
	return $output . "</optgroup>";
}

function get_menu_of_locations($con, $get) { 
	$query = 'SELECT `idlocations`, `location_name` FROM `valyrian`.`locations`;';
	$result = $con->query($query);
	$output = '';
	foreach ($result as $row) {
		$output = $output . "<option value=" . $row['idlocations'];
		if ($row['idlocations'] == $get) { $output = $output . " selected"; }
		$output = $output . ">" . $row['location_name'] . "</option>";
	}
	return $output;
}

function get_menu_of_theories($con, $get) {
	$query = 'SELECT `idtheories`, `theory_code`, `name`, `idcategories`, `category_name` FROM `valyrian`.`theories` INNER JOIN `valyrian`.`categories` ON `category` = `idcategories` ORDER BY `category`, `theory_code` ASC;';
	$result = $con->query($query);
	$output = '';
	$loc = 0;
	foreach ($result as $row) {
		if ($row['idcategories'] != $cat) {
			$output = $output . "</optgroup>\n<optgroup label=\"" . $row['category_name'] . "\">";
			$cat = $row['idcategories'];
		}
		$output = $output . "<option value=" . $row['idtheories'];
		if ($row['idtheories'] == $get) { $output = $output . " selected"; }
		$output = $output . ">" . $row['name'];
		$output = $output . "</option>";
	}
	return $output . "</optgroup>";
}

function get_char_status($status) {
	switch ($status) {
		case 1:
			return '<span style="color: #000;">Dead</span>';
		case 2:
			return 'Resurrected';
		case 3:
			return '<span class="status_good">Alive</span>';
		case 4:
			return 'Brainwashed/mad';
		default:
			return 'Unknown';
	}
}

function get_related_theories_character($id, $con) { 
	global $home;
	$result = $con->prepare('SELECT `theory_code`, `name`, `tldr` FROM `valyrian`.`theories` JOIN `valyrian`.`theories_characters` ON `theory` = `idtheories` WHERE `character` = :id;');
	$result->bindValue(':id', $id);
	$result->execute();
	$output = '';
	if ($row = $result->fetch()) {
		do {
		$output .= "<li><a href=\"{$home}theory/{$row['theory_code']}\">{$row['name']}</a></li>";
		} while ($row = $result->fetch());
	} else { $output .= 'None'; }
	return $output;
}

function get_related_characters($id, $con) {
	$result = $con->prepare('SELECT `idcharaters`, `first_name`, `last_name`, `common_name` FROM `valyrian`.`characters` JOIN `valyrian`.`theories_characters` ON `character` = `idcharaters` WHERE `theory` = :id;');
	$result->bindValue(':id', $id);
	$result->execute();
	$output = '';
	if ($row = $result->fetch()) {
		do {
		$output .= "<li>" . get_char_link($row['idcharaters'], $row['first_name'], $row['last_name'], $row['common_name']) . "</li>";
		} while ($row = $result->fetch());
	} else { $output .= 'None'; }
	return $output;
}

function get_family($mom, $dad, $spouse, $con) {
	$result = $con->prepare('SELECT `idcharaters`, `first_name`, `last_name`, `common_name` FROM `valyrian`.`characters` WHERE `idcharaters` IN ( :mom, :dad, :spouse );');
	$result->bindValue(':mom', $mom);
	$result->bindValue(':dad', $dad);
	$result->bindValue(':spouse', $spouse);
	$result->execute();
	if ($row = $result->fetch()) { ?>
		<h5 class="meta">Related characters</h5>
		<ul class="rel_people_list rel_list"><?php
		do {
		$output .= "<li><span class=\"meta\">";
		switch ($row['idcharaters']) {
			case $mom:
				 $output .= "Mother: ";
				 break;
			case $dad:
				$output .= "Father: ";
				break;
			case $spouse:
				$output .= "Spouse: ";
				break;
		}
		$output .= "</span>" . get_char_link($row['idcharaters'], $row['first_name'], $row['last_name'], $row['common_name']) . "</li>";
		} while ($row = $result->fetch());
		$output .= "</ul>";
	}// else { $output .= 'None'; }
	return $output;
}

function get_location($id, $con) {
	$result = $con->prepare('SELECT `location_name` FROM `valyrian`.`locations` WHERE `idlocations` = :id');
	$result->bindValue(':id', $id);
	$result->execute();
	if ($row = $result->fetch()) {
		return $row['location_name'];
	} else { return "Unknown"; }
}
?>
