<?php
	include 'header.php';
	
	if (!empty($_GET['start'])) { $start = $_GET['start']; } else { $start = 0; }
	if (!empty($_GET['show'])) { $show = $_GET['show']; } else { $show = 15; }
	$end = $start+$show;
?>
	
	<span class="meta">Showing category:</span>
	
	<?php
		$result = $pdo->prepare("SELECT `category_name` FROM `valyrian`.`categories` WHERE idcategories = :id");
		$result->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
		$result->execute();
		$row = $result->fetch();
		echo $row['category_name'];
		
		$result = $pdo->prepare("SELECT * FROM `valyrian`.`theories` WHERE category = :id LIMIT :start, :end");
		$result->bindValue(':start', $start, PDO::PARAM_INT);
		$result->bindValue(':end', $end, PDO::PARAM_INT);
		$result->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
		$result->execute();

	include 'loop.php';
	
	include 'pagenav.php';

	include 'footer.php'
?>
