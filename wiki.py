import requests

from time import sleep

class Wikipedia:

    url_article = 'http://awoiaf.westeros.org/index.php?action=raw&title='
    headers_r = {'User-Agent': 'Mozilla/5.0', 'From': 'tom1099pl@gmail.com'}  # This is another valid field

    def __init__(self):
        pass

    def __fetch(self, article):
        #print('Article _ftch:', article)
        sleep(3)
        #url = article.replace(' ','_')
        params_dict = {'action': 'raw', 'title': article}
        a = self.url_article + article.replace(' ','_')
        #print(a)
        response = requests.get(a.rstrip(), headers=self.headers_r)
        #response = requests.get('http://awoiaf.westeros.org/index.php?action=raw&title=Abelar_Hightower', headers=self.headers_r)
        #print(response.text)
        return response

    def unwiki(self, article):
        #print('Article unwiki:', article)
        response = self.__fetch(article)
        print(response)
        output = ''
        data = []
        article_began = infobox_ended = False
        assert isinstance(response, requests.Response)
        for j in response.iter_lines():
            i = j.decode()
            if (i.startswith('| Spouse') or i.startswith('| Title') or i.startswith('| Alias')) and not i.endswith('='):
                #print(i)
                splitted_line = i.split('|')[1].replace('|', '').replace('  ', '').replace('[', '').replace(']', '').split('=')
                splitted_line[0] = splitted_line[0].strip()
                splitted_line[1] = splitted_line[1].strip()
                data.append(splitted_line)
            elif '}}' in i:
                infobox_ended = True
            elif i.startswith('\'\'\'') or i.startswith('Ser \'\'\'') or (infobox_ended and '\'\'\'' in i):
                output += i
                article_began = True
            elif article_began and ((not i) or i.startswith('==')):
                break
        l = [output, data]
        print(l)
        #print(' ')
        #print(l)
        return l